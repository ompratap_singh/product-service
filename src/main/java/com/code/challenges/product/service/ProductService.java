package com.code.challenges.product.service;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.code.challenges.product.domain.Product;
import com.code.challenges.product.repository.IProductRepository;

@Service
public class ProductService implements IProductService {

	static Random random = new Random();

	@Autowired
	private IProductRepository productRepository;

	@Override
	public void addProduct(Product product) {
		product.setId(random.nextInt(1000));
		productRepository.saveProduct(product);
	}

	@Override
	public void editProduct(Product product) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteProduct(int id) {
		productRepository.deleteProduct(id);
	}

	@Override
	public List<Product> getAllProduct() {
		return productRepository.getAllProduct();
	}

}
