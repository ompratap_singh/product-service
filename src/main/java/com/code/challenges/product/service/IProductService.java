package com.code.challenges.product.service;

import java.util.List;

import com.code.challenges.product.domain.Product;

public interface IProductService {

	void addProduct(Product product);

	void editProduct(Product product);

	void deleteProduct(int id);

	List<Product> getAllProduct();
}
