package com.code.challenges.product.repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Repository;

import com.code.challenges.product.cache.ApplicationCache;
import com.code.challenges.product.domain.Product;

@Repository
public class ProductRepository implements IProductRepository {

	@Override
	public void saveProduct(Product product) {
		Map<Integer, Product> cacheMap = ApplicationCache.getInsTance().cacheMap;
		cacheMap.putIfAbsent(product.getId(), product);
	}

	@Override
	public void updateProduct(Product product) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteProduct(int id) {
		Map<Integer, Product> cacheMap = ApplicationCache.getInsTance().cacheMap;
		cacheMap.remove(id);
	}

	@Override
	public List<Product> getAllProduct() {
		Map<Integer, Product> cacheMap = ApplicationCache.getInsTance().cacheMap;
		if (ObjectUtils.isNotEmpty(cacheMap.values())) {
			return new ArrayList<>(cacheMap.values());
		} else {
			return Collections.emptyList();
		}

	}

}
