package com.code.challenges.product.repository;

import java.util.List;

import com.code.challenges.product.domain.Product;

public interface IProductRepository {

	void saveProduct(Product product);

	void updateProduct(Product product);

	void deleteProduct(int id);

	List<Product> getAllProduct();
}
