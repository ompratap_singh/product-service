package com.code.challenges.product.cache;

import java.util.HashMap;
import java.util.Map;

import com.code.challenges.product.domain.Product;

public class ApplicationCache {

	public Map<Integer, Product> cacheMap = new HashMap<>();

	private ApplicationCache() {
		// No Instance Allowed
	}

	private static ApplicationCache applicationCache;

	public static synchronized ApplicationCache getInsTance() {
		if (applicationCache == null) {
			applicationCache = new ApplicationCache();
		}
		return applicationCache;
	}
}
