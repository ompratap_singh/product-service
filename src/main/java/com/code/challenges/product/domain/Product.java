package com.code.challenges.product.domain;

import static com.code.challenges.product.util.Constants.NON_IMAGE_URL_PATTERN;
import static com.code.challenges.product.util.Constants.NON_SPECIAL_CHARACTER_PATTERN;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Product {

	private int id;

	private long price;

	@NotNull
	@Size(max = 100)
	@Pattern(regexp = NON_SPECIAL_CHARACTER_PATTERN, message = "{validation.msg.name}")
	private String name;

	@NotNull
	@Size(max = 400)
	@Pattern(regexp = NON_SPECIAL_CHARACTER_PATTERN, message = "{validation.msg.description}")
	private String description;

	@NotNull
	@Pattern(regexp = NON_IMAGE_URL_PATTERN)
	private String imageUrl;

	private String colors;
	
	@NotNull(message = "{validation.msg.custom.attribute}")
	@Size(min = 1)
	private List<CustomAttribute> customAttributes;

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getColors() {
		return colors;
	}

	public void setColors(String colors) {
		this.colors = colors;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<CustomAttribute> getCustomAttributes() {
		return customAttributes;
	}

	public void setCustomAttributes(List<CustomAttribute> customAttributes) {
		this.customAttributes = customAttributes;
	}

}
