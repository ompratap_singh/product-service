package com.code.challenges.product.resource;

import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.code.challenges.product.domain.Product;
import com.code.challenges.product.service.IProductService;
import com.code.challenges.product.util.ValidationUtil;

@RestController
@RequestMapping("v1/product")
public class ProductController {

	@Autowired
	private IProductService productService;

	@PostMapping(value = "/add", consumes = "application/json")
	public ResponseEntity<String> addProduct(@RequestBody Product product) {
		if (ObjectUtils.isNotEmpty(product) && ValidationUtil.validateEntity(product) == 0) {
			productService.addProduct(product);
			return new ResponseEntity<>("Resource Created", HttpStatus.CREATED);
		} else {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Attribute.");
		}
	}

	@GetMapping(value = "/fetch-all")
	public List<Product> getAllProduct() {
		try {
			return productService.getAllProduct();
		} catch (Exception exception) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Process Failed.");
		}

	}

	@PutMapping(value = "update")
	public void updateProduct(@RequestBody Product product) {
		// TODO Implementation pending
	}

	@DeleteMapping(value = "{id}/delete")
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public void deleteProduct(@PathVariable(name = "id") int id) {
		productService.deleteProduct(id);
	}

}
