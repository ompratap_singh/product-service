package com.code.challenges.product.util;

public final class Constants {

	private Constants() {
		// No instance allowed
	}

	public static final String NON_IMAGE_URL_PATTERN = "(?:([^:/?#]+):)?(?://([^/?#]*))?([^?#]*\\.(?:jpg|gif|png))(?:\\?([^#]*))?(?:#(.*))?";
	
	public static final String NON_SPECIAL_CHARACTER_PATTERN = "^[^\\\\\\\\?*!@&#$%+={}\\\".,|`~\\\\[\\\\]<>^]*$";
	
}
