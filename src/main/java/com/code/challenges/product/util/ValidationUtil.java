package com.code.challenges.product.util;

import java.util.Collections;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.util.CollectionUtils;

public class ValidationUtil {

	private ValidationUtil() {
		// No Instance allowed
	}

	private static Validator validator;

	static {
		ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
		validator = validatorFactory.getValidator();
	}

	public static int validateEntity(Object target) {
		Set<ConstraintViolation<Object>> constraintViolations = validator.validate(target);
		if (!CollectionUtils.isEmpty(constraintViolations)) {
			System.out.println(constraintViolations.toString());
			return constraintViolations.size();
		} else {
			return Collections.emptySet().size();
		}
	}
}
